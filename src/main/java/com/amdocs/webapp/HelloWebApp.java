package com.amdocs.webapp;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWebApp extends HttpServlet {

	private BasicCalculator calc;

	private static final long serialVersionUID = 1L;

        protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
	         throws ServletException, IOException {
            resp.setContentType("text/plain");
            resp.getWriter().write("Hello Limassol! \n\n\nJava Project for DevOps hands-on Workshop.\n\n\n" +
		"Testing a new line addition, to verify CI/CD Pipeline.\n\n\n" +
		"Testing a new line addition, to verify the deployment of the artifacts to the two Docker Tomcat containers.\n\n\n" +
		"Testing a new line addition, to verify that the integration test using Selenium, works.\n\n\nAndreas Neofytou\n\n\n");
        }


	public HelloWebApp() {
		this.calc = new BasicCalculator();
	}

	public HelloWebApp(BasicCalculator calc) {
		this.calc = calc;
	}

	public int avg(int a, int b) {
	    return calc.add(a, b)/2;
	}

}
